/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./client/js/utils/htmlUtils.js":
/*!**************************************!*\
  !*** ./client/js/utils/htmlUtils.js ***!
  \**************************************/
/***/ ((module) => {

eval("function createHTMLElementFromString(htmlString) {\n    const div = document.createElement('div');\n    div.innerHTML = htmlString.trim();\n\n    return div.firstChild;\n}\n\nfunction clearNode(node) {\n    // eslint-disable-next-line no-param-reassign\n    node.innerHTML = '';\n}\n\nmodule.exports = {\n    createHTMLElementFromString,\n    clearNode\n};\n\n\n//# sourceURL=webpack://boot-camp/./client/js/utils/htmlUtils.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
(() => {
/*!****************************!*\
  !*** ./client/js/index.js ***!
  \****************************/
eval("const htmlUtils = __webpack_require__(/*! ./utils/htmlUtils */ \"./client/js/utils/htmlUtils.js\");\n\nconst productGrid = document.getElementsByClassName('js-product-grid')[0];\n\nasync function showProducts(url) {\n    const response = await window.fetch(url, {\n        method: 'GET'\n    });\n\n    if (response.status === 200) {\n        htmlUtils.clearNode(productGrid);\n        const text = await response.text();\n        productGrid.innerHTML = text;\n    }\n}\n\nfunction showQuickView(url) {\n    window.fetch(`${window.location.origin}${url}`, {\n        method: 'GET'\n    }).then((response) => {\n        if (response.status === 200) {\n            const modal = document.querySelector('.js-modal');\n            const body = modal.querySelector('.js-modal-body');\n            htmlUtils.clearNode(body);\n\n            response.text().then((text) => {\n                body.innerHTML = text;\n                modal.classList.add('b-page__modal--open');\n            });\n        }\n    });\n}\n\nfunction initEvents() {\n    const sortingSelector = document.querySelector('.js-sorting');\n    if (sortingSelector) {\n        sortingSelector.addEventListener('change', (event) => {\n            const url = event.currentTarget.options[event.currentTarget.selectedIndex].attributes['data-url'].value;\n            showProducts(url);\n        });\n    }\n\n    const searchForm = document.querySelector('.js-search');\n    searchForm.addEventListener('submit', (event) => {\n        event.preventDefault();\n        const { value } = event.currentTarget.querySelector('input');\n        if (value && value !== '') {\n            const url = `${searchForm.action}?q=${value}`;\n            window.location = url;\n        }\n    });\n\n    document.addEventListener('click', (event) => {\n        if (event.target.classList.contains('js-quick-view')) {\n            event.preventDefault();\n            const url = event.target.attributes['data-url'].value;\n\n            showQuickView(url);\n        }\n\n        if (event.target.classList.contains('js-close-modal')) {\n            event.preventDefault();\n            event.target.closest('.js-modal').classList.remove('b-page__modal--open');\n        }\n    });\n}\n\nfunction app() {\n    initEvents();\n}\n\napp();\n\n\n//# sourceURL=webpack://boot-camp/./client/js/index.js?");
})();

/******/ })()
;